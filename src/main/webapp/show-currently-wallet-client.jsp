<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@page isELIgnored="false" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ include file="konfiguration/link.jsp" %>
    <%@ include file="navBarTop.jsp" %>
</head>
<body>

<div id="connectingPanel" class="container-fluid p-4">
    <h2 style="text-align: center"> View client wallet</h2>
    <br/>
    <div class="row">
        <div class="col-5">
            <div class="row">
                <div class="mr-1 col container-fluid bg-dark p-2" style="height: 250px;color: orange;">
                    <h4 style="text-align: center">PABU792939</h4>
                    <dl>
                        <dt>Rabat</dt>
                        <dd>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25"
                                     aria-valuemin="0" aria-valuemax="100">25%
                                </div>
                            </div>
                        </dd>
                        <dt>Info 1</dt>
                        <dt>Info 2</dt>
                        <dt>Info 3</dt>
                        <dt>Info 4</dt>
                    </dl>
                </div>
                <button type="button" class="mt-1 mr-1 btn btn-info  btn-lg btn-block">NEW SELL</button>
            </div>
        </div>
        <div class="col-7 container-fluid bg-dark p-2" style="color: orange; height: 300px">
            <div class="row" style="position: relative">
                <div class="col-3 " style="text-align: center">Name:</div>
                <div class="col-3 " style="text-align: center">Amount:</div>
                <div class="col-3 " style="text-align: center">Time of use:</div>
                <div class="col-3" style="text-align: center">Prince:</div>
            </div>
            <hr style="background: orange">

            <div class="row" style="position: relative">
                <div class="col-3 " style="text-align: center">Name</div>
                <div class="col-3 " style="text-align: center">50</div>
                <div class="col-3 " style="text-align: center">6</div>
                <div class="col-3" style="text-align: center">600</div>
            </div>

            <div class="row" style="position: relative">
                <div class="col-3 " style="text-align: center">Second</div>
                <div class="col-3 " style="text-align: center">80</div>
                <div class="col-3 " style="text-align: center">30</div>
                <div class="col-3" style="text-align: center">90</div>
            </div>

        </div>

    </div>
</div>
<footer>
    <div> 2020 &copy; Product by D38M0N</div>
    <%@ include file="konfiguration/script.jsp" %>
</footer>
</body>
</html>
