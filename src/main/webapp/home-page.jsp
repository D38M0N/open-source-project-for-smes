<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@page isELIgnored="false" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ include file="konfiguration/link.jsp" %>
    <%@ include file="navBarTop.jsp"%>
</head>
<body>

<img src="konfiguration/LogoCSBD.jpg" style=" width:100px; margin-top: 20px;  opacity: 70%;" class="rounded mx-auto d-block shadow-lg" alt="LogoCSBD">
<div id="startPanel" class="container-fluid p-4" style="  position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    width:350px;
    transform: translate(-50%, -50%);
    opacity: 80%;
    border-radius: 5px;">

    <form>
        <div class="row">
            <div class="col-9 p-0">
                <input type="text" class="form-control" placeholder="ID CLIENT">
            </div>
            <div class="col-3 p-0">
                <button type="submit" class="btn btn-warning">CHECK</button>

            </div>
        </div>
    </form>

</div>

<footer>
    <div> 2020 &copy; Product by D38M0N</div>
    <%@ include file="konfiguration/script.jsp" %>
</footer>
</body>
</html>
