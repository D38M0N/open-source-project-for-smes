<html>
<head>
    <%@page isELIgnored="false" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ include file="konfiguration/link.jsp" %>
    <%@ include file="navBarTop.jsp"%>
</head>
<body>

<div class="container-fluid p-4" style="position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    width: 900px;
    transform: translate(-50%, -50%);
    background: #FFFFCC;
    opacity: 80%;
    border-radius: 5px;">

    <h1 style="text-align: center"> Create your services and products</h1>

    <form class="needs-validation" novalidate>
        <div class="form-row">
            <div class="col-md-2 mb-3">
                <label for="brand_code">BRAND CODE</label>
                <input type="text" class="form-control" id="brand_code" value="" required>
                <div class="valid-tooltip">
                    Looks good!
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <label for="full_name">Full name</label>
                <input type="text" class="form-control" id="full_name" value="" required>
                <div class="valid-tooltip">
                    Looks good!
                </div>
            </div>

            <div class="col-md-2 mb-3">
                <label for="basic_units">Basic units:</label>
                <select class="custom-select" id="basic_units" required>
                    <option selected disabled value="">Choose...</option>
                    <option>Hour</option>
                    <option>Kilogram</option>
                    <option>Liter</option>
                    <option>Package</option>
                    <option>Art</option>
                    <option>Service</option>

                </select>
            </div>
            <div class="col-md-2 mb-3">
                <label for="basic_units">Duty</label>
                <select class="custom-select" id="category" required>
                    <option selected disabled value="">Choose...</option>
                    <option>8%</option>
                    <option>23%</option>
                </select>
            </div>
            <div class="col-md-2 mb-3">
                <label for="price">Price </label>
                <input type="text" class="form-control" id="price" value="0" required>
                <div class="valid-tooltip">
                    Looks good!
                </div>
            </div>
        </div>


        <div class="form-row">
            <div class="col-md-4 mb-4">
                <div>
                    <label for="spinner">Amount</label>
                    <input id="spinner" name="spinner" value="0.00">
                </div>
                <br/>
                <div class="col-10">
                    <label for="slider">Duration of use (months):</label>
                    <div id="slider">
                        <div id="custom-handle" style=" width: 3em;
    height: 1.6em;
    top: 50%;
    margin-top: -.8em;
    text-align: center;
    line-height: 1.6em;" class="ui-slider-handle"></div>
                    </div>
                </div>

            </div>
            <div class="col-md-3 mb-3">
                <div>
                <label for="datepicker">Expiration date</label>
                <input type="text" id="datepicker">
                </div>
                <br/>
                <div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="warranty" value="warranty">
                        <label class="form-check-label" for="warranty">Warranty</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="invoice" value="invoice">
                        <label class="form-check-label" for="invoice">Invoice</label>
                    </div>

                </div>

                </div>
            <div class="col-md-5 mb-3">
                <label for="summary">Summary</label>
                <textarea class="form-control" id="summary" rows="3"></textarea>
                <div class="invalid-tooltip">
                    Please select a valid state.
                </div>
            </div>
        </div>

        <button class="btn btn-primary btn-block " type="submit">Save your service / product</button>
    </form>

</div>
<footer>
    <div> 2020 &copy; Product by D38M0N</div>
    <%@ include file="konfiguration/script.jsp" %>
</footer>
</body>

</html>
