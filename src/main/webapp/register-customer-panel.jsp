<html>
<head>
    <%@page isELIgnored="false" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ include file="konfiguration/link.jsp" %>
</head>
<body>
<div id="registerPanel" class="container-fluid p-4">
    <h2 style="text-align: center">Register Panel</h2>
    <form>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmailReg">Register e-mail </label>
                <input type="email" class="form-control" id="inputEmailReg" placeholder="Email">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPasswordReg">Create Password</label>
                <input type="password" class="form-control" id="inputPasswordReg" placeholder="Create Password">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputCity">City</label>
                <input type="text" class="form-control" id="inputCity">
            </div>
            <div class="form-group col-md-4">
                <label for="inputState">Category activity </label>
                <select id="inputState" class="form-control">
                    <option selected>Choose...</option>
                    <option> Serviceman</option>
                    <option> Dealer</option>
                    <option> Teacher</option>
                    <option> Other</option>
                </select>
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="statute">
                <label class="form-check-label" for="statute"><a href="" data-toggle="modal"
                                                                 data-target="#statuteModal">Read and check Statute</a></label>
            </div>
        </div>
        <button type="submit" class="btn btn-block btn-primary">Register</button>
    </form>


</div>

<div class="modal fade" id="statuteModal" tabindex="-1" role="dialog" aria-labelledby="statuteModalText"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="statuteModalText">D38M0N Statute</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ornare auctor arcu, in pharetra
                    tortor
                    euismod eget. Morbi tortor urna, molestie nec tincidunt nec, pretium quis odio. Lorem ipsum dolor
                    sit
                    amet, consectetur adipiscing elit. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                    Suspendisse quis tortor tempor, feugiat tortor id, euismod felis. Ut ut eros rutrum odio ullamcorper
                    fringilla. Maecenas in posuere arcu. Sed lobortis purus a ultrices tristique. Nullam ut congue
                    ligula.
                    Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus
                    commodo dolor ligula, id pretium mi eleifend a. Curabitur at vulputate purus, ut lacinia eros. Ut
                    lacinia pellentesque sem non ultrices.
                </p>
                <p>
                    Maecenas id nisl ut magna sagittis euismod sit amet vel lectus. Sed ullamcorper sapien et nulla
                    viverra,
                    at dictum tellus varius. Nunc nunc felis, auctor nec eleifend eget, commodo in nunc. Class aptent
                    taciti
                    sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Interdum et malesuada fames
                    ac
                    ante ipsum primis in faucibus. Maecenas pharetra, nibh ac sodales aliquet, mi turpis condimentum
                    nunc,
                    tristique lacinia tellus massa tincidunt massa. Aenean imperdiet magna nec congue sollicitudin.
                </p>
                <p>
                    Nulla risus leo, ornare at tortor id, pulvinar mattis turpis. Vivamus id ornare purus, id congue
                    neque.
                    Mauris tristique, eros nec consectetur maximus, diam nulla elementum arcu, a cursus leo dui non
                    lacus.
                    Suspendisse gravida mattis tellus quis pharetra. Proin in mattis turpis, et eleifend ipsum.
                    Curabitur
                    lobortis congue malesuada. Sed non lacus in magna ullamcorper tempor. Praesent gravida magna in
                    dolor
                    mattis fermentum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                    mus.
                    Morbi est risus, ornare hendrerit auctor nec, eleifend eu neque. Proin ac nisi ut ligula commodo
                    auctor
                    eget eu sem. Sed nec eros sit amet metus malesuada vulputate.
                </p>
                <p>
                    Duis ac pharetra mi. Sed id malesuada velit. Morbi leo orci, tempor quis luctus sed, dapibus sit
                    amet
                    mi. Nulla euismod non neque ac euismod. Aenean ac libero metus. Phasellus massa arcu, sagittis at
                    laoreet id, consequat et lectus. Nullam ut ipsum urna. Nam dignissim sapien sed velit mollis
                    consectetur. Duis at elementum purus. Vestibulum vel eros euismod metus tristique condimentum. Duis
                    sem
                    dui, bibendum in gravida ut, maximus non nibh. Praesent maximus mi leo, ac maximus erat viverra sit
                    amet. Nullam vitae nunc eu est feugiat bibendum vitae nec nisi.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<footer>
    <div> 2020 &copy; Product by D38M0N</div>
    <%@ include file="konfiguration/script.jsp" %>
</footer>
</body>

</html>
