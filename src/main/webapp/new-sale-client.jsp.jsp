<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@page isELIgnored="false" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ include file="konfiguration/link.jsp" %>
    <%@ include file="navBarTop.jsp" %>
</head>
<body>

<div id="connectingPanel" class="container-fluid p-4">
    <div class="row">

        <div class=" col-3" style="text-align: center; margin-top: 19px">
            <span class="border border-dark p-3"> PABU-792939 </span>

        </div>

        <select class=" col-9 custom-select bg-light border border-dark " style="height: 350px; margin-outside: 5px" multiple>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
            <option selected>
                <div class="row" style="position: relative">
                    <div class="col-3">Name: Mower repair</div>
                    <div class="col-3">Amount:99</div>
                    <div class="col-3">Time of use:12</div>
                    <div class="col-3">Prince:990</div>
                </div>
            </option>
        </select>

        <button type="submit" class=" btn btn-primary btn-lg btn-block">SELL</button>

    </div>
</div>
<footer>
    <div> 2020 &copy; Product by D38M0N</div>
    <%@ include file="konfiguration/script.jsp" %>
</footer>
</body>
</html>
