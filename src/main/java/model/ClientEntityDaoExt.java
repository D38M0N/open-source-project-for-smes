package model;

import model.dao.ManagerDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ClientEntityDaoExt {
    private static Logger logger = LoggerFactory.getLogger(ClientEntityDaoExt.class);
    private EntityManagerFactory entityManagerFactory;

    public ClientEntityDaoExt() {
        entityManagerFactory = Persistence.createEntityManagerFactory("model");
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public void close() {
        entityManagerFactory.close();
    }

    public static void main(String[] args) {
        ClientEntityDaoExt dao = new ClientEntityDaoExt();
        ManagerDAO em = new ManagerDAO(dao.getEntityManagerFactory());
        try {
            em.autoFillDataBase();
        } catch (Exception e) {
            logger.error("", e);
        } finally {
            dao.close();
        }
    }

}


