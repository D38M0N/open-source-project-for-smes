package model.type;

public enum ProductsType {
    FAT_BURN,
    BODY_BUILD,
    ENERGY,
    PRE_WORKOUT;
}
