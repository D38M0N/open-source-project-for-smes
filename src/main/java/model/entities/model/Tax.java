package model.entities.model;

public enum Tax {
    NONE(0),
    EIGHT(8),
    TWENTY_THREE(23);
    private int tax;

    Tax(int tax) {
        this.tax = tax;
    }

    public int getTaxNumber() {
        return tax;
    }
}
