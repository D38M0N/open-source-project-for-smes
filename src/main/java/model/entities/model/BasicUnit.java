package model.entities.model;

public enum BasicUnit {
  NONE,
    HOUR,
    KILOGRAM,
    LITER,
    PACKAGE,
    ART,
    SERVICE;
}
