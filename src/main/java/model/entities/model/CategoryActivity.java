package model.entities.model;

public enum CategoryActivity {
    NONE(""),
    SERVICEMAN("Serviceman"),
    DEALER("Dealer"),
    TEACHER("Teacher"),
    OTHER("OTHER");
    private String name;

    CategoryActivity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
