package model.entities;

import javax.persistence.*;

@Entity
@Table(name = "basic_information")
public class BasicInformation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String discount;
    private String sumOfBought;
    private String frequencyVisit;
    private String status;


    public BasicInformation() {
    }

    public BasicInformation(String error) {
        this.discount = error;
    }

    public BasicInformation(String discount, String sumOfBought, String frequencyVisit, String status) {
        this.discount = discount;
        this.sumOfBought = sumOfBought;
        this.frequencyVisit = frequencyVisit;
        this.status = status;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String bi_1) {
        this.discount = bi_1;
    }

    public String getSumOfBought() {
        return sumOfBought;
    }

    public void setSumOfBought(String bi_2) {
        this.sumOfBought = bi_2;
    }

    public String getFrequencyVisit() {
        return frequencyVisit;
    }

    public void setFrequencyVisit(String bi_3) {
        this.frequencyVisit = bi_3;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String bi_4) {
        this.status = bi_4;
    }
}
