package model.entities;

import model.entities.model.BasicUnit;
import model.entities.model.Tax;

import javax.persistence.*;

@Entity
@Table(name = "item_for_sell")
public class ItemForSell {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_item;
    private String BRAND_CODE = "empty";
    private String FULL_NAME = "empty";
    private BasicUnit basicUnit = BasicUnit.NONE;
    private Tax tax = Tax.NONE;
    private double prince = 0.0;
    private int AMOUNT = 0;
    private String EXPIRATION_DATE = "empty";
    private String SUMMARY = "empty";
    private int DURATION = 0;
    private boolean WARRANTY = false;
    private boolean INVOICE = false;

    public ItemForSell() {
    }


    public ItemForSell addBrandCode(String brandCode) {
        this.BRAND_CODE = brandCode;
        return this;
    }

    public ItemForSell addFullName(String fullName) {
        this.FULL_NAME = fullName;
        return this;
    }

    public ItemForSell addBasicUnit(BasicUnit basicUnit) {
        this.basicUnit = basicUnit;
        return this;
    }

    public ItemForSell addDuty(Tax tax) {
        this.tax = tax;
        return this;
    }

    public ItemForSell addPrince(double prince) {
        this.prince = prince;
        return this;
    }

    public ItemForSell addAmount(int amount) {
        this.AMOUNT += amount;
        return this;
    }
    public ItemForSell lessAmount(int amount) {
        this.AMOUNT -= amount;
        return this;
    }

    public ItemForSell addExpirationDate(String expirationDate) {
        this.EXPIRATION_DATE = expirationDate;
        return this;
    }

    public ItemForSell addSummary(String summary) {
        this.SUMMARY = summary;
        return this;

    }
   public ItemForSell addToCurrentlySummary(String summary){
        this.SUMMARY += summary;
        return this;
    }

    public ItemForSell addDuration(int duration) {
        this.DURATION = duration;
        return this;
    }

    public ItemForSell addWarranty() {
        this.WARRANTY = true;
        return this;
    }

    public ItemForSell addInvoice() {
        this.INVOICE = true;
        return this;
    }
    public ItemForSell deleteWarranty() {
        this.WARRANTY = false;
        return this;
    }

    public ItemForSell deleteInvoice() {
        this.INVOICE = false;
        return this;
    }

    public int getId_item() {
        return id_item;
    }

    public String getBRAND_CODE() {
        return BRAND_CODE;
    }

    public String getFULL_NAME() {
        return FULL_NAME;
    }

    public BasicUnit getBasicUnit() {
        return basicUnit;
    }

    public Tax getTax() {
        return tax;
    }

    public double getPrince() {
        return prince;
    }

    public int getAMOUNT() {
        return AMOUNT;
    }

    public String getEXPIRATION_DATE() {
        return EXPIRATION_DATE;
    }

    public String getSUMMARY() {
        return SUMMARY;
    }

    public int getDURATION() {
        return DURATION;
    }

    public boolean isWARRANTY() {
        return WARRANTY;
    }

    public boolean isINVOICE() {
        return INVOICE;
    }
}
