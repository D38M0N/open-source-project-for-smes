package model.entities;

import javax.persistence.*;

@Entity
@Table(name = "additional_information")
public class AdditionalInformation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_additional_information;

    private String ia_1;
    private String ia_2;
    private String ia_3;
    private String ia_4;

    protected AdditionalInformation() {
    }

    public AdditionalInformation(String errorMess) {
        this.ia_1 = errorMess;
    }

    public AdditionalInformation(String ia_1, String ia_2, String ia_3, String ia_4) {
        this.ia_1 = ia_1;
        this.ia_2 = ia_2;
        this.ia_3 = ia_3;
        this.ia_4 = ia_4;
    }

    public String getIa_1() {
        return ia_1;
    }

    public void setIa_1(String ia_1) {
        this.ia_1 = ia_1;
    }

    public String getIa_2() {
        return ia_2;
    }

    public void setIa_2(String ia_2) {
        this.ia_2 = ia_2;
    }

    public String getIa_3() {
        return ia_3;
    }

    public void setIa_3(String ia_3) {
        this.ia_3 = ia_3;
    }

    public String getIa_4() {
        return ia_4;
    }

    public void setIa_4(String ia_4) {
        this.ia_4 = ia_4;
    }
}
