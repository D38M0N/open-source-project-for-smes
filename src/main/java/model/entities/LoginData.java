package model.entities;

import model.entities.model.CategoryActivity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
public class LoginData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String email;
    private String pwd;
    private CategoryActivity categoryActivity;
    private String city;

    @ManyToMany(cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinTable(name = "user_clients",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "client_id")})
    private Set<ClientEntity> clientEntities = new HashSet<>();
    protected LoginData() {
    }

    public int getId() {

        return id;
    }

    public LoginData addClient(ClientEntity clientEntity) {
        this.clientEntities.add(clientEntity);
        return this;
    }

    public LoginData(String email, String pwd, CategoryActivity categoryActivity, String city) {
        this.email = email;
        this.pwd = passwordConverter(pwd);
        this.categoryActivity = categoryActivity;
        this.city = city;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPwd(String pwd) {
        this.pwd = passwordConverter(pwd);
    }

    public void setCategoryActivity(CategoryActivity categoryActivity) {
        this.categoryActivity = categoryActivity;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public static String passwordConverter(String a) {
        String modification = "d38m0n2020";
        String feedBack = "";
        for (int i = 0; i < a.length(); i++) {
            for (int k = 0; k < modification.length(); k++) {
                feedBack += (char) (a.charAt(i) ^ modification.charAt(k));
            }
        }
        return feedBack;
    }

    public String getEmail() {
        return email;
    }

    public String getPwd() {
        return pwd;
    }

    public CategoryActivity getCategoryActivity() {
        return categoryActivity;
    }

    public String getCity() {
        return city;
    }

    public Set<ClientEntity> getClientEntities() {
        return clientEntities;
    }
}

