package model.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "client")
public class ClientEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_Client;
    private String privatePersonID;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private BasicInformation basicInformation;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private AdditionalInformation additionalInformation;

    @ManyToMany(cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinTable(name = "clients_products",
            joinColumns = {@JoinColumn(name = "client_id")},
            inverseJoinColumns = {@JoinColumn(name = "item_id")})
    private Set<ItemForSell> itemForSell = new HashSet<>();

    protected ClientEntity() {
    }

    public ClientEntity(String privatePersonID) {
        this.privatePersonID = privatePersonID;


    }
public ClientEntity addBI(BasicInformation basicInformation){
    this.basicInformation = basicInformation;
        return this;
}
public ClientEntity addIA(AdditionalInformation additionalInformation){
    this.additionalInformation = additionalInformation;
        return this;
}

    public ClientEntity addSold(ItemForSell itemForSell){
        this.itemForSell.add(itemForSell);
        return this;
    }

    public String getPrivatePersonID() {
        return privatePersonID;
    }

    public void setPrivatePersonID(String privatePersonID) {
        this.privatePersonID = privatePersonID;
    }

    public BasicInformation getBasicInformation() {
        return basicInformation;
    }

    public void setBasicInformation(BasicInformation basicInformation) {
        this.basicInformation = basicInformation;
    }

    public AdditionalInformation getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(AdditionalInformation additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public Set<ItemForSell> getItemForSell() {
        return itemForSell;
    }

    public void setItemForSell(Set<ItemForSell> itemForSell) {
        this.itemForSell = itemForSell;
    }

    public int getId_Client() {
        return id_Client;
    }
}
