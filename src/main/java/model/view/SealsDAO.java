package model.view;

public interface SealsDAO {
   void sealServiceWithOtherPrince(int id_Service, double otherPrince);
   void sealStandardService(int id_Service);
   void sealProductWithOtherPrice(int id_Product,double otherPrince);
   void sealStandardProduct(int id_Product);


}
