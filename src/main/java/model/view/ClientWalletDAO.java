package model.view;

public interface ClientWalletDAO {

    String getInformation_3();

    String getInformation_2();

    String getInformation_1();

    String getInformation_0( );

    double getDiscount();
}
