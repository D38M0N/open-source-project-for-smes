package model.dao;

import model.entities.BasicInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.sql.SQLException;

public class BasicInformationImp implements BasicInformationDAO {
    private EntityManager entityManager;
    private static Logger logger = LoggerFactory.getLogger(ClientImp.class);

    public BasicInformationImp(EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();

    }

    @Override
    public void saveBasicInformationDao(BasicInformation basicInformation) {

        try {

            entityManager.getTransaction().begin();
            entityManager.persist(basicInformation);
            logger.info("OK. I  ADD NEW Basic Information");

        } finally {
            entityManager.getTransaction().commit();
        }
    }

    @Override
    public BasicInformation findBI(int id_BI) throws SQLException {
        BasicInformation basicInformation = null;

        try {
            entityManager.getTransaction().begin();
            basicInformation = entityManager.find(BasicInformation.class, id_BI);

            logger.info("***** Find successfully  - I Load Basic Information *****");

        } catch (NoResultException noresult) {
            logger.error("not result find Basic Information", noresult);
            basicInformation = new BasicInformation("Sorry I can't find it ");
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);

        } finally {
            entityManager.getTransaction().commit();
        }
        return basicInformation;

    }

    @Override
    public void delete(int id_BI) throws SQLException {

        try {

            entityManager.getTransaction().begin();
            BasicInformation basicInformation = entityManager.find(BasicInformation.class, id_BI);
            logger.info("***** Find successfully  - I Load Basic Information *****");
            entityManager.remove(basicInformation);
            logger.info("Complete delete ");

        } finally {
            entityManager.getTransaction().commit();
        }
    }

    @Override
    public void upDateDiscount(int id_BI, String changeInf) throws SQLException {
        try {

            entityManager.getTransaction().begin();
            BasicInformation basicInformation = entityManager.find(BasicInformation.class, id_BI);
            logger.info("***** Find successfully  - I Load Basic Information *****");
            basicInformation.setDiscount(changeInf);
            logger.info("changed basic information -- > discount");
            entityManager.merge(basicInformation);
            logger.info("Complete merge discount. ");

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void upDateSumOfBought(int id_BI, String changeInf) throws SQLException {
        try {

            entityManager.getTransaction().begin();
            BasicInformation basicInformation = entityManager.find(BasicInformation.class, id_BI);
            logger.info("***** Find successfully  - I Load Basic Information *****");
            basicInformation.setSumOfBought(changeInf);
            logger.info("changed basic information -- > sum of bought");
            entityManager.merge(basicInformation);
            logger.info("Complete merge sum of bought.");

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void upDateFrequencyVisit(int id_BI, String changeInf) throws SQLException {
        try {

            entityManager.getTransaction().begin();
            BasicInformation basicInformation = entityManager.find(BasicInformation.class, id_BI);
            logger.info("***** Find successfully  - I Load Basic Information *****");
            basicInformation.setFrequencyVisit(changeInf);
            logger.info("changed basic information -- > frequency visit");
            entityManager.merge(basicInformation);
            logger.info("Complete merge frequency visit.");

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void upDateStatus(int id_BI, String changeInf) throws SQLException {

        try {

            entityManager.getTransaction().begin();
            BasicInformation basicInformation = entityManager.find(BasicInformation.class, id_BI);
            logger.info("***** Find successfully  - I Load Basic Information *****");
            basicInformation.setStatus(changeInf);
            logger.info("changed basic information -- > Status");
            entityManager.merge(basicInformation);
            logger.info("Complete merge status.");

        } finally {
            entityManager.getTransaction().commit();
        }

    }


}
