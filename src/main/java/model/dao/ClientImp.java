package model.dao;

import model.entities.ClientEntity;
import model.entities.ItemForSell;
import org.hibernate.tool.schema.extract.internal.IndexInformationImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.sql.SQLException;

public class ClientImp implements ClientDAO {
    private EntityManager entityManager;
    private static Logger logger = LoggerFactory.getLogger(ClientImp.class);

    public ClientImp(EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void saveNewClient(ClientEntity clientEntity) throws SQLException {

        String ppID = clientEntity.getPrivatePersonID();
        try {
            entityManager.getTransaction().begin();
            entityManager.createQuery("from ClientEntity where privatePersonID = :PrivatePersonID", ClientEntity.class)
                    .setParameter("PrivatePersonID", ppID)
                    .getSingleResult();

            logger.info("***** Find successfully *****");

        } catch (NoResultException noresult) {
            logger.debug("OK. I  ADD NEW CLIENT ");
            entityManager.persist(clientEntity);
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);

        } finally {
            entityManager.getTransaction().commit();
        }


    }


    @Override
    public ClientEntity findClient(String id_PrivatePersonID) throws SQLException {
        ClientEntity entity = null;

        try {
            entityManager.getTransaction().begin();
            entity = entityManager.createQuery("from ClientEntity where privatePersonID = :PrivatePersonID", ClientEntity.class)
                    .setParameter("PrivatePersonID", id_PrivatePersonID)
                    .getSingleResult();

            logger.info("***** Find successfully *****");

        } catch (NoResultException noresult) {
            logger.error("not result find ClientEntity", noresult);
            entity = new ClientEntity("Sorry I can't find it ");
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);

        } finally {
            entityManager.getTransaction().commit();
        }
        return entity;
    }

    @Override
    public void delete(int id_Person) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ClientEntity clientEntity = entityManager.find(ClientEntity.class, id_Person);
            entityManager.remove(clientEntity);

        } finally {
            entityManager.getTransaction().commit();
        }
    }


}
