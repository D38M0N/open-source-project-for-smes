package model.dao;

import model.entities.ItemForSell;

import model.entities.model.BasicUnit;
import model.entities.model.Tax;
import org.hibernate.tool.schema.extract.internal.IndexInformationImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.sql.SQLException;


public class ItemForSellImp implements ItemForSellDAO {
    private EntityManager entityManager;
    private static Logger logger = LoggerFactory.getLogger(ItemForSellImp.class);

    public ItemForSellImp(EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void createItem(ItemForSell itemForSell) {
        try {
            entityManager.getTransaction().begin();
            entityManager.createQuery("from ItemForSell where BRAND_CODE = :brandValue", ItemForSell.class)
                    .setParameter("brandValue", itemForSell.getBRAND_CODE())
                    .getSingleResult();

            logger.warn("Sorry!!! This email is used");


        } catch (NoResultException noresult) {
            logger.info("Success add item for sell !!!!!!!!! **********");
            entityManager.persist(itemForSell);
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);
        } finally {
            entityManager.getTransaction().commit();
        }
    }

    @Override
    public void deleteItem(int id_item) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            entityManager.remove(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }
    }


    @Override
    public ItemForSell findItem(String brand_code) throws SQLException {
        ItemForSell itemForSell = null;
        try {
            entityManager.getTransaction().begin();
            itemForSell = entityManager.createQuery("from ItemForSell where BRAND_CODE = :brandValue", ItemForSell.class)
                    .setParameter("brandValue", brand_code)
                    .getSingleResult();

            logger.info("***** Find successfully *****");

        } catch (NoResultException noresult) {
            logger.error("not result", noresult);
            itemForSell = new ItemForSell().addBrandCode("Sorry I can't find it");
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);

        } finally {
            entityManager.getTransaction().commit();
        }
        return itemForSell;
    }

    @Override
    public void changBrandCode(int id_item, String brandCode_new) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addBrandCode(brandCode_new);
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }
    }

    @Override
    public void changName(int id_item, String name_new) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addFullName(name_new);
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void changBasicUnit(int id_item, BasicUnit basicUnit_new) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addBasicUnit(basicUnit_new);
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void changTax(int id_item, Tax tax_new) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addDuty(tax_new);
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void changPrince(int id_item, double prince_new) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addPrince(prince_new);
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void addAmount(int id_item, int howMuchAdd) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addAmount(howMuchAdd);
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void lessAmount(int id_item, int howMuchLess) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.lessAmount(howMuchLess);
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }


    @Override
    public void changeExpirationDate(int id_item, String dateOfEnd_new) throws SQLException {

        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addExpirationDate(dateOfEnd_new);
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void changeSummary(int id_item, String summary_new) throws SQLException {

        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addSummary(summary_new);
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void addToCurrentlySummary(int id_item, String summary_add) throws SQLException {

        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addToCurrentlySummary(summary_add);
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void changeDuration(int id_item, int duration_new) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addDuration(duration_new);
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void turnOnWarrant(int id_item) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addWarranty();
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void turnOffWarrant(int id_item) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.deleteWarranty();
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void turnOnInvoice(int id_item) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.addInvoice();
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void turnOffInvoice(int id_item) throws SQLException {
        try {
            entityManager.getTransaction().begin();
            ItemForSell itemForSell = entityManager.find(ItemForSell.class, id_item);
            itemForSell.deleteInvoice();
            entityManager.merge(itemForSell);

        } finally {
            entityManager.getTransaction().commit();
        }

    }

}
