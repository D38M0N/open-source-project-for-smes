package model.dao;

import model.entities.LoginData;
import model.entities.model.CategoryActivity;

import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;

public class ManagerDAO {
    private EntityManagerFactory entityManagerFactory;

    public ManagerDAO(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public void autoFillDataBase() throws SQLException {

        LoginDateDAO loginDateDAO = new LoginDateImp(entityManagerFactory);

//      loginDateDAO.createNewUser(new LoginData("Oweasadr2", "sa", CategoryActivity.NONE, "KKs"));

//        loginDateDAO.restartPassword("Ower2");

//        LoginData data = new LoginData("Ower2", "00000", CategoryActivity.NONE, "KKs");
//        loginDateDAO.changePassword(data,"sa");
//        loginDateDAO.changeActivity(31, CategoryActivity.OTHER);
//        loginDateDAO.changeEmail(31,"Ower3");
//        loginDateDAO.changeCity(31,"Kakow");
//    loginDateDAO.findUser("Ower3", "00000");
        for(int i =11 ; i<46;i++) {
            loginDateDAO.deleteUser(i);
        }
    }
}
