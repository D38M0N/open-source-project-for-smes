package model.dao;

import model.entities.AdditionalInformation;

import java.sql.SQLException;

public interface AdditionalInformationDAO {
    void saveInformationAdditionalDAO(AdditionalInformation additionalInformation) throws SQLException;
    void upDate_IA(AdditionalInformation additionalInformation, int id_informationAdditional) throws SQLException;
    AdditionalInformation find_IA(int id_informationAdditional) throws SQLException;
    void delete(int id_informationAdditional) throws SQLException;
    void upDate_ia_1(int id_informationAdditional, String upDate_ia) throws SQLException;
    void upDate_ia_2(int id_informationAdditional, String upDate_ia) throws SQLException;
    void upDate_ia_3(int id_informationAdditional, String upDate_ia) throws SQLException;
    void upDate_ia_4(int id_informationAdditional, String upDate_ia) throws SQLException;
}
