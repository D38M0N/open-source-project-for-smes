package model.dao;

import model.entities.ItemForSell;
import model.entities.model.BasicUnit;
import model.entities.model.Tax;

import java.sql.SQLException;

public interface ItemForSellDAO {
    void createItem(ItemForSell itemForSell) throws SQLException;

    void deleteItem(int id_item) throws SQLException;

    ItemForSell findItem(String brand_code) throws SQLException;

    void changBrandCode(int id_item, String brandCode_new) throws SQLException;

    void changName(int id_item, String name_new) throws SQLException;

    void changBasicUnit(int id_item, BasicUnit basicUnit_new) throws SQLException;

    void changTax(int id_item, Tax tax_new) throws SQLException;

    void changPrince(int id_item, double prince_new) throws SQLException;

    void addAmount(int id_item, int howMuchAdd) throws SQLException;

    void lessAmount(int id_item, int howMuchLess) throws SQLException;

    void changeExpirationDate(int id_item, String dateOfEnd_new) throws SQLException;

    void changeSummary(int id_item, String summary_new) throws SQLException;
    void addToCurrentlySummary(int id_item, String summary_add) throws SQLException;

    void changeDuration(int id_item, int duration_new) throws SQLException;

    void turnOnWarrant(int id_item) throws SQLException;

    void turnOffWarrant(int id_item) throws SQLException;

    void turnOnInvoice(int id_item) throws SQLException;

    void turnOffInvoice(int id_item) throws SQLException;

}
