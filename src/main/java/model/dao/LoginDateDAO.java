package model.dao;

import model.entities.LoginData;
import model.entities.model.CategoryActivity;

import java.sql.SQLException;

public interface LoginDateDAO {
    void createNewUser(LoginData loginData) throws SQLException;

    void restartPassword(String email) throws SQLException;

    void changePassword(LoginData loginData, String pwdNew) throws SQLException;

    void changeEmail(int id_user, String email) throws SQLException;

    void changeCity(int id_user, String City) throws SQLException;

    void changeActivity(int id_user, CategoryActivity categoryActivity) throws SQLException;

    void deleteUser(int id_user) throws SQLException;

    LoginData findUser(String email, String pwd) throws SQLException;

}
