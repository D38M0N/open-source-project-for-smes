package model.dao;

import model.entities.LoginData;
import model.entities.model.CategoryActivity;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.sql.SQLException;

import org.slf4j.Logger;

import static model.entities.LoginData.passwordConverter;


public class LoginDateImp implements LoginDateDAO {

    private static Logger logger = LoggerFactory.getLogger(LoginDateImp.class);
    private EntityManager entityManager;

    public LoginDateImp(EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void createNewUser(LoginData loginData) throws SQLException {

        try {
            entityManager.getTransaction().begin();
            entityManager.createQuery("from LoginData where email = :emailValue ", LoginData.class)
                    .setParameter("emailValue", loginData.getEmail())
                    .getSingleResult();
            logger.warn("Sorry!!! This email is used");

        } catch (NoResultException noresult) {
            entityManager.merge(loginData);
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);
        } finally {
            entityManager.getTransaction().commit();
        }
    }

    @Override
    public void restartPassword(String email) throws SQLException {
        String randomPassword = "00000";
        try {
            entityManager.getTransaction().begin();

            int id = entityManager.createQuery("from LoginData where email = :emailValue ", LoginData.class)
                    .setParameter("emailValue", email)
                    .getSingleResult().getId();

            LoginData loginData = entityManager.find(LoginData.class, id);
            loginData.setPwd(randomPassword);
            entityManager.merge(loginData);
        } catch (NoResultException noresult) {
            logger.error("not result", noresult);
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);
        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void changePassword(LoginData loginData, String pwdNew) throws SQLException {

        try {
            entityManager.getTransaction().begin();
            int id = entityManager.createQuery("from LoginData where email = :emailValue and categoryActivity = :caValue and city = :cityValue and pwd = :pwdValue ", LoginData.class)
                    .setParameter("emailValue", loginData.getEmail())
                    .setParameter("caValue", loginData.getCategoryActivity())
                    .setParameter("cityValue", loginData.getCity())
                    .setParameter("pwdValue", loginData.getPwd())
                    .getSingleResult()
                    .getId();

            LoginData ld_changed = entityManager.find(LoginData.class, id);
            ld_changed.setPwd(pwdNew);
            entityManager.merge(ld_changed);

            logger.info("******* password change successful *********");

        } catch (NoResultException noresult) {
            logger.error("not result", noresult);
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);
        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void changeEmail(int id_user, String email) throws SQLException {

        try {
            entityManager.getTransaction().begin();
            LoginData ld_changed = entityManager.find(LoginData.class, id_user);
            ld_changed.setEmail(email);
            entityManager.merge(ld_changed);

            logger.info("***** Save new e-mail successfully *****");

        } catch (NoResultException noresult) {
            logger.error("not result", noresult);
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);
        } finally {
            entityManager.getTransaction().commit();
        }


    }

    @Override
    public void changeCity(int id_user, String city) throws SQLException {

        try {
            entityManager.getTransaction().begin();
            LoginData ld_changed = entityManager.find(LoginData.class, id_user);
            ld_changed.setCity(city);
            entityManager.merge(ld_changed);

            logger.info("***** Save new city successfully *****");

        } catch (NoResultException noresult) {
            logger.error("not result", noresult);
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);
        } finally {
            entityManager.getTransaction().commit();
        }
    }

    @Override
    public void changeActivity(int id_user, CategoryActivity categoryActivity) throws SQLException {

        try {
            entityManager.getTransaction().begin();
            LoginData ld_changed = entityManager.find(LoginData.class, id_user);
            ld_changed.setCategoryActivity(categoryActivity);
            entityManager.merge(ld_changed);

            logger.info("***** Save new category activity successfully *****");

        } catch (NoResultException noresult) {
            logger.error("not result", noresult);
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);
        } finally {
            entityManager.getTransaction().commit();
        }
    }

    @Override
    public void deleteUser(int id_user) throws SQLException {

        try {
            entityManager.getTransaction().begin();
            LoginData ld_changed = entityManager.find(LoginData.class, id_user);
            entityManager.remove(ld_changed);
            logger.info("***** Delete user successfully *****");

        } finally {
            entityManager.getTransaction().commit();
        }
    }

    @Override
    public LoginData findUser(String email, String pwd) throws SQLException {
        LoginData findUser = null;

        try {
            entityManager.getTransaction().begin();
            findUser = entityManager.createQuery("from LoginData where email = :emailValue and pwd = :pwdValue ", LoginData.class)
                    .setParameter("emailValue", email)
                    .setParameter("pwdValue", passwordConverter(pwd))
                    .getSingleResult();

            logger.info("***** Find successfully *****");

        } catch (NoResultException noresult) {
            logger.error("not result", noresult);
            return null;
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);
            return null;
        } finally {
            entityManager.getTransaction().commit();
        }
        return findUser;
    }


}

