package model.dao;

import model.entities.ClientEntity;

import java.sql.SQLException;


public interface ClientDAO {
    void saveNewClient(ClientEntity clientEntity) throws SQLException;
    ClientEntity findClient (String id_Person) throws SQLException;
    void delete(int id_Person) throws SQLException;

}
