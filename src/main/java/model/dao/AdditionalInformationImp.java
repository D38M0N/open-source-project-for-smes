package model.dao;

import model.entities.AdditionalInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.sql.SQLException;

public class AdditionalInformationImp implements AdditionalInformationDAO {
    private EntityManager entityManager;
    private static Logger logger = LoggerFactory.getLogger(ClientImp.class);


    public AdditionalInformationImp(EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void saveInformationAdditionalDAO(AdditionalInformation additionalInformation) {
        try {

            entityManager.getTransaction().begin();
            entityManager.persist(additionalInformation);
            logger.info("OK. I  ADD NEW Additional Information");

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void upDate_IA(AdditionalInformation additionalInformation, int id_informationAdditional) throws SQLException {
        {

            try {

                entityManager.getTransaction().begin();
                AdditionalInformation additionalFind = entityManager.find(AdditionalInformation.class, id_informationAdditional);
                logger.info("***** Find successfully  - I Load Information Additional1 *****");

                additionalFind.setIa_1(additionalInformation.getIa_1());
                additionalFind.setIa_2(additionalInformation.getIa_2());
                additionalFind.setIa_3(additionalInformation.getIa_3());
                additionalFind.setIa_4(additionalInformation.getIa_4());


                entityManager.merge(additionalFind);
                logger.info("Complete up date additional. ");

            } finally {
                entityManager.getTransaction().commit();
            }
        }
    }

    @Override
    public AdditionalInformation find_IA(int id_informationAdditional) throws SQLException {
        AdditionalInformation ia_find = null;

        try {
            entityManager.getTransaction().begin();
            ia_find = entityManager.find(AdditionalInformation.class, id_informationAdditional);

            logger.info("***** Find successfully  - I Load Basic Information *****");

        } catch (NoResultException noresult) {
            logger.error("not result find Additional Information", noresult);
            ia_find = new AdditionalInformation("Sorry I can't find it ");
        } catch (NonUniqueResultException notUnique) {
            logger.error("more than one result", notUnique);

        } finally {
            entityManager.getTransaction().commit();
        }
        return ia_find;

    }

    @Override
    public void delete(int id_informationAdditional) throws SQLException {
        {

            try {

                entityManager.getTransaction().begin();
                AdditionalInformation additionalFind = entityManager.find(AdditionalInformation.class, id_informationAdditional);
                logger.info("***** Find successfully  - I Load Information Additional1 *****");
                entityManager.remove(additionalFind);
                logger.info("delete additional. ");

            } finally {
                entityManager.getTransaction().commit();
            }
        }
    }

    @Override
    public void upDate_ia_1(int id_informationAdditional, String upDate_ia) throws SQLException {
        try {

            entityManager.getTransaction().begin();
            AdditionalInformation additionalInformation = entityManager.find(AdditionalInformation.class, id_informationAdditional);
            logger.info("***** Find successfully  - I Load Information Additional  *****");
            additionalInformation.setIa_1(upDate_ia);
            logger.info("changed Information Additional -- > 1");
            entityManager.merge(additionalInformation);
            logger.info("Complete merge up date position 1");

        } finally {
            entityManager.getTransaction().commit();
        }
   }

    @Override
    public void upDate_ia_2(int id_informationAdditional, String upDate_ia) throws SQLException {
        try {

            entityManager.getTransaction().begin();
            AdditionalInformation additionalInformation = entityManager.find(AdditionalInformation.class, id_informationAdditional);
            logger.info("***** Find successfully  - I Load Information Additional  *****");
            additionalInformation.setIa_2(upDate_ia);
            logger.info("changed Information Additional -- > 2");
            entityManager.merge(additionalInformation);
            logger.info("Complete merge up date position 2");

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void upDate_ia_3(int id_informationAdditional, String upDate_ia) throws SQLException {
        try {

            entityManager.getTransaction().begin();
            AdditionalInformation additionalInformation = entityManager.find(AdditionalInformation.class, id_informationAdditional);
            logger.info("***** Find successfully  - I Load Information Additional  *****");
            additionalInformation.setIa_3(upDate_ia);
            logger.info("changed Information Additional -- > 3");
            entityManager.merge(additionalInformation);
            logger.info("Complete merge up date position 3");

        } finally {
            entityManager.getTransaction().commit();
        }

    }

    @Override
    public void upDate_ia_4(int id_informationAdditional, String upDate_ia) throws SQLException {
        try {

            entityManager.getTransaction().begin();
            AdditionalInformation additionalInformation = entityManager.find(AdditionalInformation.class, id_informationAdditional);
            logger.info("***** Find successfully  - I Load Information Additional  *****");
            additionalInformation.setIa_4(upDate_ia);
            logger.info("changed Information Additional -- > 4");
            entityManager.merge(additionalInformation);
            logger.info("Complete merge up date position 4");

        } finally {
            entityManager.getTransaction().commit();
        }

    }

}
