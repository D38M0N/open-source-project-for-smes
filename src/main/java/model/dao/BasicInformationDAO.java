package model.dao;

import model.entities.BasicInformation;

import java.sql.SQLException;

public interface BasicInformationDAO {
    void saveBasicInformationDao(BasicInformation basicInformation) throws SQLException;

    BasicInformation findBI(int id_BI) throws SQLException;

    void delete(int id_BI) throws SQLException;

    void upDateDiscount(int id_BI, String changeInf) throws SQLException;

    void upDateSumOfBought(int id_BI, String changeInf) throws SQLException;

    void upDateFrequencyVisit(int id_BI, String changeInf) throws SQLException;

    void upDateStatus(int id_BI, String changeInf) throws SQLException;


}
