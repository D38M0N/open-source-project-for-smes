package model;

import model.entities.ClientEntity;

import java.sql.SQLException;

public interface PersonManagerDAO {
    void addPerson(ClientEntity clientEntity) throws SQLException;
    void update(String id_Person) throws SQLException;
    void delete(String id_Person) throws SQLException;
    void choosePersonalDateBase(String id_Personal) throws SQLException;

}
