package model.dao;

import model.ClientEntityDaoExt;
import model.entities.ClientEntity;
import model.entities.ItemForSell;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class ClientImpTest {
    ClientEntity clientEntity;
    ClientImp testClientImp;

    @Before
    public void setUp() throws Exception {

        testClientImp = new ClientImp(new ClientEntityDaoExt().getEntityManagerFactory());
        testClientImp.saveNewClient(new ClientEntity("Testing methods"));
        clientEntity = testClientImp.findClient("Testing methods");

    }

    @After
    public void tearDown() throws Exception {
        testClientImp.delete(clientEntity.getId_Client());
    }

    @Test
    public void delete() throws SQLException {
        testClientImp.saveNewClient(new ClientEntity("Testing delete"));
        ClientEntity testing_delete = testClientImp.findClient("Testing delete");
        testClientImp.delete(testing_delete.getId_Client());
        ClientEntity testing_delete_AFTER = testClientImp.findClient("Testing delete");
        assertEquals(testing_delete_AFTER.getPrivatePersonID(), "Sorry I can't find it ");
    }

    @Test
    public void saveNewClient() throws SQLException {
        testClientImp.saveNewClient(new ClientEntity("Testing delete"));
        ClientEntity testing_delete = testClientImp.findClient("Testing delete");
        assertEquals(testing_delete.getPrivatePersonID(), "Testing delete");
        testClientImp.delete(testing_delete.getId_Client());
    }


    @Test
    public void findClient() {
        assertTrue(clientEntity != null);
    }

}