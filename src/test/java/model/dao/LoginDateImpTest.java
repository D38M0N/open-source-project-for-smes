package model.dao;

import model.ClientEntityDaoExt;
import model.entities.LoginData;
import model.entities.model.CategoryActivity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class LoginDateImpTest {
    ClientEntityDaoExt dao;
    LoginDateImp testLDI;
    LoginData userTest;

    @Before
    public void setUp() throws Exception {
        dao = new ClientEntityDaoExt();
        testLDI = new LoginDateImp(dao.getEntityManagerFactory());
        LoginData loginData = new LoginData("testEmail", "test", CategoryActivity.NONE, "testCity");
        testLDI.createNewUser(loginData);
        userTest = testLDI.findUser("testEmail", "test");
    }

    @After
    public void tearDown() throws Exception {
        testLDI.deleteUser(userTest.getId());
    }

    @Test
    public void createNewUser() throws SQLException {
        assertNotEquals(null, userTest);
        assertEquals("testEmail", userTest.getEmail());
        assertEquals(CategoryActivity.NONE, userTest.getCategoryActivity());
        assertEquals("testCity", userTest.getCity());

    }

    @Test
    public void restartPassword() throws SQLException {

        testLDI.restartPassword(userTest.getEmail());
        LoginData userAfter = testLDI.findUser(userTest.getEmail(), "00000");

        assertEquals(userAfter.getEmail(), userTest.getEmail());
        assertEquals(userAfter.getId(), userTest.getId());
        assertNotEquals(userAfter.getPwd(), userTest);

    }

    @Test
    public void changePassword() throws SQLException {

        testLDI.changePassword(userTest, "123456");
        LoginData userAfter = testLDI.findUser("testEmail", "123456");
        assertEquals(userAfter.getEmail(), userTest.getEmail());
        assertEquals(userAfter.getId(), userTest.getId());

    }

    @Test
    public void changeEmail() throws SQLException {

        testLDI.changeEmail(userTest.getId(), "AfterEmail");
        LoginData userAfter = testLDI.findUser("AfterEmail", "test");

        assertEquals("AfterEmail", userAfter.getEmail());
        assertEquals(userAfter.getId(), userTest.getId());


    }

    @Test
    public void changeCity() throws SQLException {
        String beforeCity = userTest.getEmail();

        testLDI.changeCity(userTest.getId(), "AfterCity");
        LoginData userAfter = testLDI.findUser("testEmail", "test");

        assertEquals("AfterCity", userAfter.getCity());
        assertEquals(userAfter.getId(), userTest.getId());
        assertNotEquals(userAfter.getCity(), beforeCity);

    }

    @Test
    public void changeActivity() throws SQLException {

        String beforeActivity = userTest.getCategoryActivity().getName();
        testLDI.changeActivity(userTest.getId(), CategoryActivity.DEALER);
        LoginData userAfter = testLDI.findUser("testEmail", "test");

        assertEquals("Dealer", userAfter.getCategoryActivity().getName());
        assertEquals(userAfter.getId(), userTest.getId());
        assertNotEquals(userAfter.getCategoryActivity().getName(), beforeActivity);

    }

    @Test
    public void deleteUser() throws SQLException {
        LoginData loginData = new LoginData("delete", "test", CategoryActivity.NONE, "testCity");
        testLDI.createNewUser(loginData);
        LoginData user = testLDI.findUser("delete", "test");
        assertNotEquals(user.getId(), null);
        testLDI.deleteUser(user.getId());
        LoginData LDOut = testLDI.findUser("delete", "test");
        assertEquals(LDOut, null);
    }

    @Test
    public void findUser() throws SQLException {

        assertNotEquals(userTest.getId(), null);

    }
}