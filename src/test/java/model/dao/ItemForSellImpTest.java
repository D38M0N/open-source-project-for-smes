package model.dao;

import model.ClientEntityDaoExt;
import model.entities.ItemForSell;
import model.entities.model.BasicUnit;
import model.entities.model.Tax;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class ItemForSellImpTest {

    ItemForSellImp testIFS;
    ItemForSell itemForSellTest;

    @Before
    public void setUp() throws Exception {

        testIFS = new ItemForSellImp(new ClientEntityDaoExt().getEntityManagerFactory());
        testIFS.createItem(new ItemForSell().addBrandCode("test"));
        itemForSellTest = testIFS.findItem("test");
    }

    @After
    public void tearDown() throws Exception {
        testIFS.deleteItem(itemForSellTest.getId_item());
    }

    @Test
    public void createItem() throws SQLException {
        assertEquals(testIFS.findItem("test").getBRAND_CODE(), "test");

    }

    @Test
    public void findItem() throws SQLException {

        assertNotEquals(testIFS.findItem("test"), null);
        assertEquals(testIFS.findItem("test"), itemForSellTest);
        assertEquals("Sorry I can't find it", testIFS.findItem("test2").getBRAND_CODE());
    }

    @Test
    public void changBrandCode() throws SQLException {

        testIFS.changBrandCode(itemForSellTest.getId_item(), "test2");
        assertEquals(testIFS.findItem("test2").getBRAND_CODE(), "test2");
        assertNotEquals(testIFS.findItem("test2").getBRAND_CODE(), "test");


    }

    @Test
    public void changName() throws SQLException {

        testIFS.changName(itemForSellTest.getId_item(), "test2");
        assertEquals(testIFS.findItem("test").getFULL_NAME(), "test2");
        assertNotEquals(testIFS.findItem("test").getFULL_NAME(), "test");

    }

    @Test
    public void changBasicUnit() throws SQLException {
        testIFS.changBasicUnit(itemForSellTest.getId_item(), BasicUnit.ART);
        assertEquals(testIFS.findItem("test").getBasicUnit(), BasicUnit.ART);

    }

    @Test
    public void changTax() throws SQLException {
        assertEquals(testIFS.findItem("test").getTax().getTaxNumber(), 0);

        testIFS.changTax(itemForSellTest.getId_item(), Tax.EIGHT);
        assertEquals(testIFS.findItem("test").getTax().getTaxNumber(), 8);
        assertNotEquals(testIFS.findItem("test").getTax().getTaxNumber(), 0);
    }

    @Test
    public void changPrince() throws SQLException {
        assertTrue(itemForSellTest.getPrince() == 0.0);
        testIFS.changPrince(itemForSellTest.getId_item(), 20.0);
        assertTrue(itemForSellTest.getPrince() == 20.0);

    }

    @Test
    public void addAmount() {
        assertTrue(itemForSellTest.getAMOUNT() == 0);
        itemForSellTest.addAmount(1);
        assertTrue(itemForSellTest.getAMOUNT() == 1);
        itemForSellTest.addAmount(3);
        assertTrue(itemForSellTest.getAMOUNT() == 4);
    }

    @Test
    public void lessAmount() {
        assertTrue(itemForSellTest.getAMOUNT() == 0);
        itemForSellTest.addAmount(4);
        assertTrue(itemForSellTest.getAMOUNT() == 4);
        itemForSellTest.lessAmount(3);
        assertTrue(itemForSellTest.getAMOUNT() == 1);
    }


    @Test
    public void turnOnWarrant() {
        assertFalse(itemForSellTest.isWARRANTY());
        itemForSellTest.addWarranty();
        assertTrue(itemForSellTest.isWARRANTY());
    }

    @Test
    public void turnOffWarrant() {
        assertFalse(itemForSellTest.isWARRANTY());
        itemForSellTest.addWarranty();
        assertTrue(itemForSellTest.isWARRANTY());
        itemForSellTest.deleteWarranty();
        assertFalse(itemForSellTest.isWARRANTY());
    }

    @Test
    public void turnOnInvoice() {

        assertFalse(itemForSellTest.isINVOICE());
        itemForSellTest.addInvoice();
        assertTrue(itemForSellTest.isINVOICE());
    }

    @Test
    public void turnOffInvoice() {

        assertFalse(itemForSellTest.isINVOICE());

        itemForSellTest.addInvoice();
        assertTrue(itemForSellTest.isINVOICE());

        itemForSellTest.deleteInvoice();
        assertFalse(itemForSellTest.isINVOICE());
    }

    @Test
    public void addToCurrentlySummary() {
        assertEquals(itemForSellTest.getSUMMARY(), "empty");
        assertNotEquals(itemForSellTest.getSUMMARY(), "empty helo helo");

        itemForSellTest.addToCurrentlySummary(" helo helo");
        assertEquals(itemForSellTest.getSUMMARY(), "empty helo helo");
    }

    @Test
    public void changeExpirationDate() {
        assertEquals(itemForSellTest.getEXPIRATION_DATE(), "empty");
        itemForSellTest.addExpirationDate("10-02-1993");
        assertEquals(itemForSellTest.getEXPIRATION_DATE(), "10-02-1993");
    }

    @Test
    public void changeSummary() {
        assertEquals(itemForSellTest.getSUMMARY(), "empty");
        itemForSellTest.addSummary("helo helo");
        assertEquals(itemForSellTest.getSUMMARY(), "helo helo");
    }


    @Test
    public void changeDuration() {

        assertTrue(itemForSellTest.getDURATION() == 0);
        itemForSellTest.addDuration(5);

        assertTrue(itemForSellTest.getDURATION() == 5);

    }
}