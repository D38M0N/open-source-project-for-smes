<html>
<head>
    <%@page isELIgnored="false" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ include file="konfiguration/link.jsp" %>
</head>
<body>

<div id="logPanel" class="container-fluid p-4">
    <form>
        <div class="form-group">
            <label for="exampleInputEmail1">E-mail</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                   placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
        </div>
    </form>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#">Register now </a>
    <a class="dropdown-item" href="#">Forgot password?</a>
</div>
</div>
<footer>
    <div> 2020 &copy; Product by D38M0N</div>
    <%@ include file="konfiguration/script.jsp" %>
</footer>
</body>

</html>
